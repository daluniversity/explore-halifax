# Explore Halifax

![Halifax waterfront](images/halifax.png)

> CSCI 3130 (Software Engineering) is one of the courses that I (Tushar Sharma) teach at Dalhousie University. As a part of the course, I took a lecture on git and GitHub. After the session, I gave the students an assignment - to contribute to a GitHub repository, i.e., "Explore Halifax". In the assignment, each student had to write about the city they live in. My primary goal was to help them understand how to use git and GitHub (since they are supposed to contribute to a single file in the repository). As a (pleasant) side effect, the assignment has prepared a comprehensive list of things to do/see/experience in Halifax, NS, Canada.

You may find the [open-source repository here](https://gitlab.com/daluniversity/explore-halifax); you are welcome to contribute more.
Thanks to all students and contributors who made this list awesome.
<a name="introduction"></a>
**Table of contents**
- [Historical places](#history)
- [Tourist spots](#tourism)
- [Art galleries](#art)
- [Museums](#museums)
- [Parks](#parks)
- [Beaches](#beaches)
- [Tours](#tours)
- [Sports & competitions](#sports)
- [Hikes](#hikes)
- [Gyms](#gyms)
- [Live Music](#music)
- [Entertainment](#entertainment)
- [Events](#events)
- [Night Clubs](#nightc-lubs)
- [Restaurants](#restaurants)
    - [Indian](#indian)
    - [Chinese](#chinese)
    - [Mediterranean](#mediterranean)
    - [Japnese and Korean](#jap_korean)
    - [Italian](#italian)
    - [Formal](#formal)
    - [Fast food](#fast-food)
    - [Others](#others)
- [Coffee and tea](#coffee)
- [Wineries and bars](#bars)
- [Dessert Shops](#desserts)
- [Government services](#govt)
- [Buildings](#buildings)
- [Libraries](#libraries)
- [Supermarkets](#supermarkets)
- [Shopping Centers](#shopping)
- [Farmers Markets](#farmers)
- [Academic institutions](#universities)
- [Learning opportunities](#learning)
- [Transportation](#transportation)


## Historical places <a name="history"></a>

### Citadel Hill
![Citadel Hill](images/citadel.jpg) <br/>
[Image credit](https://www.tripadvisor.com/Attractions-g154976-Activities-c47-Halifax_Halifax_Regional_Municipality_Nova_Scotia.html)

There are a lot of Historical Places to visit in Halifax. The Citadel Hill tops the list. It is a hill that has a harbor underneath itself. At Citadel Hill, visitors can explore the history of the fortress and the soldiers stationed there as well as touch on a piece of Halifax's military history. More information about it could be found [here](https://www.novascotia.com/see-do/attractions/halifax-citadel-national-historic-site/1440), [here](https://www.halifaxcitadel.ca/), and [here](https://www.pc.gc.ca/en/lhn-nhs/ns/halifax).


### Point Pleasant park
[Point Pleasant park](https://www.novascotia.com/see-do/attractions/point-pleasant-park/1461) has a few military historical sites and a nice Atlantic ocean view.

### York Redoubt
[York Redoubt](https://www.pc.gc.ca/en/lhn-nhs/ns/york) is an old military defense base. The site offers a fabulous view of Halifax.

### Canadian Forces Base
[Canadian Forces Base](https://www.cafconnection.ca/Halifax/In-My-Community/About-CFB-Halifax.aspx) (CFB) is one of the largest Canadian military bases and home to Canada's East Coast Navy. It was founded in 1906. Now Canadian Forces Base supports Maritime Forces Atlantic and allocated lodger units with administrative, logistics, information technology, port operations and emergency services.

### Dingle Tower
[Dingle Tower](https://scotiasites.com/dingle-tower/) was created to commemorate the 150th anniversary of the establishment of the government in Nova Scotia. The tower was built from 1908-1912. You can get a nice view of the Northwest Arm from the top of the tower.

### Fairview Lawn Cemetery
![Fairview Cemetery](images/fairviewCemetery.jpg)
[Image credit](https://en.wikipedia.org/wiki/Fairview_Lawn_Cemetery)

[Fairview Lawn Cemetery](https://en.wikipedia.org/wiki/Fairview_Lawn_Cemetery) located at [3720 Windsor St](https://goo.gl/maps/EgGptiasJj1m8hgc6) is a cemetery best known for being the final resting place of 121 victims from the sinking of the Titanic. The cemetery also contains 29 war graves of Commonwealth service personnel.

## Tourist spots <a name="tourism"></a>
### Victoria Park
[Victoria Park](https://en.wikipedia.org/wiki/Victoria_Park,_Halifax,_Nova_Scotia) is a small park that seems like not much at first glance. However, this park is home to a lot of wildlife with a special abundance of pigeons. If you are ever bored and looking for a fun time, visit the Dollarama down the street on the spring garden and pick up a bag of bird seed, and you will make many friends in this park.

### Petit Passage Whale Watching
[Petit Passage Whale Watching](https://www.ppww.ca) is a place to watch real whales.

### George's Island
[George's Island](https://www.pc.gc.ca/en/lhn-nhs/ns/georges/visit) This national park is a quaint little island in the Halifax harbor. Accessible only by boat (or harbor hopper!), this island is rich in history and biodiversity as it is reportedly [infested](https://www.saltwire.com/nova-scotia/news/halifaxs-historic-snake-laden-island-will-be-open-to-the-public-next-summer-340147/) with snakes!

### Halifax Town Clock
[Halifax Town Clock](https://www.pc.gc.ca/apps/dfhd/page_fhbro_eng.aspx?id=10277) is one of the most recognizable landmarks in the historic urban core of Halifax. Situated nearby Citadel Hill, tourists love to go to the Town Clock to get a better view of the landscape of downtown Halifax.

### Peggy's Cove
[Peggy's Cove Lighthouse](https://www.novascotia.com/see-do/attractions/peggys-cove-village-and-lighthouse/1468) Peggy's Cove Lighthouse, also known as Peggy's Point Lighthouse, is one of Nova Scotia's most well-known lighthouses and may be the most photographed in Canada. (from Nova Scotia website)

### Halifax Waterfront
[Halifax Waterfront](https://discoverhalifaxns.com/explore/halifax-waterfront/) is a popular tourist attraction spot. The waterfront contains a boardwalk that stretches several kilometers. This location also houses several popular restaurants and iconic structures like 'The Wave'.


## Art galleries <a name="art"></a>

### Art gallery of Nova Scotia
The [Art gallery of Nova Scotia](https://www.artgalleryofnovascotia.ca/), with locations in downtown Halifax and Yarmouth, is the largest art museum in Atlantic Canada.

### Studio 21
![Studio 21](images/studio21.jpg)
[Image credit](https://www.novascotia.com/see-do/fine-arts/studio-21-fine-art/2623)

[Studio 21](https://studio21.ca/) is an art studio located at [5431 Doyle St #102](https://g.page/Studio21FineArt?share) featuring Canadian artists, with half of the artists being from the Atlantic region. The exhibitions change monthly and have shipping available worldwide.


## Museums

### Thomas Mcculloch Museum
![Museum](images/whale.jpg)

[Image credit](https://mapsus.net/CA/thomas-mcculloch-museum-188625)

Though Halifax has many museums, one of the most accessible museums is within Dalhousie University. It is called Thomas Mcculloch Museum. This museum resides in the Department of Biology in the Life Science Building at Dalhousie. This museum has one of the biggest collections of birds, beetles, butterflies, moths, fishes, and mushroom species. One of the most interesting facts about the museum is that it houses the vertebrate piece of Indian whale and the museum itself, followed by Aquatron. More information about the museum could be found [here](https://www.dal.ca/faculty/science/biology/research/facilities/thomas-mcculloch-museum.html).


### The Canadian museum of immigration
[The Canadian museum of immigration](https://pier21.ca/) is located next to the seaport farmers market. The museum details Halifax's long history as a landing port for immigrants. Pier 21 is a National Historic site and it is site of Atlantic Canada's only national museum.

### Discovery Centre
[Discovery Centre](https://thediscoverycentre.ca/) is a fun and interactive science museum in Halifax.

### Museum of Natural History
[Museum of Natural History](https://naturalhistory.novascotia.ca/) a kind of science it can help people know the natural history

### Maritime Museum of the Atlantic
[Martime Museum of the Atlantic](https://maritimemuseum.novascotia.ca/) offers maritime history deeply interwoven into the heart of Halifax and Nova Scotia. Explore everything from how the Mi'kmaw peoples interacted with the ocean, the days of sailing, to WW1 and WW2 era Canadian naval history. Has the world's most important wooden Titanic artifacts.

### Beyond Van Gogh Halifax
[Beyond Van Gogh Halifax](https://vangoghhalifax.com/) is an art museum that is truly an immersive experience. If you are artistic, you will love this as the floors, ceilings, and walls transform into art. The atmosphere causes you to feel like you have ventured directly into a Van Gogh painting.

## Parks

### Africville Park

[Africville Park](https://www.pc.gc.ca/apps/dfhd/page_nhs_eng.aspx?id=1763) used to be the location of an African Nova Scotian neighborhood before it was demolished for urban renewal. It now hosts a community park, including a mountain biking circuit for all to enjoy.

### Point Pleasant Park
It is a nice park at the South End (near the port). At this park, people can walk， ride and feel the sea breeze and nature, and there are several small fortresses hidden in the woods; this is a great place to relax in your free time.

### Admiral Cove Park
![Admiral Cove Park](images/park.jpeg) <br/>
[Image credit](https://www.halifaxtrails.ca/admiral-cove-park/)

[Admiral Cove Park](https://www.halifaxtrails.ca/admiral-cove-park/) is a beautiful hidden spot in Bedford Halifax with an amazing view and many trails in the mountains for hiking.


### Fort Needham Park
[Fort Needham Park](https://www.halifax.ca/parks-recreation/arts-culture-heritage/halifax-explosion/fort-needham-memorial-park-master-plan) is a newly renovated park in the historic Hydrostone neighborhood of North End Halifax. This park is dedicated to the lives lost in the Halifax Explosion of 1917, and boasts a state-of-the-art playground for children of all ages.

### Halifax Common
[Halifax Common](https://www.google.com/maps/place/Halifax+Common/@44.6499143,-63.5893351,17.58z/data=!4m5!3m4!1s0x4b5a222ad81d169d:0x381aec3ed7597adf!8m2!3d44.6499674!4d-63.5889051) is a beautiful urban park located in the center of Halifax. If you're visiting in the winter, check out the outdoor skating rink at Emera Oval.


### Long Lake Provincial Park
[Long Lake Provincial Park](https://www.halifaxtrails.ca/long-lake-provincial-park/) is a beautiful park located on the outskirts of Halifax. This park is famous for its long trail where people go on walking usually.
It's a great place to go canoeing. It's located on the western side of Halifax on Dunbrack street. If you would like to stroll, do an adventurous hike, paddle, bike ride or swim, then this is the place for you.


### DeWolf Park
[DeWolf Park](https://www.tripadvisor.ca/Attraction_Review-g910762-d12817003-Reviews-DeWolf_Park-Bedford_Halifax_Regional_Municipality_Nova_Scotia.html) is a wonderful place for a walk! People walk along the waterfront and enjoy the sites. There is a nice park area if you have children; they usually have fun events in the summer months.

### Horseshoe Island Park
[Horseshoe Island Park](https://www.yelp.ca/biz/horseshoe-island-park-halifax) is on 6939 Upper Grove, Halifax, NS B3H 2M6. It is a small park and has some small benches. You can enjoy the beautiful view of sunrise and sunset here.

### Public Gardens
[This beautiful park](https://www.halifaxpublicgardens.ca/) is home to a wide variety of plant life, statues, many ducks, and even a tiny scale model of the Titanic floating in a pond. It also has decent Wi-Fi!


### Hail Pond Park
Hail Pond Park is 3.6 km away from Dalhousie University. It is a beautiful place for a morning walk or to take your dog a walk.

### Oakfield park
[Oakfield park](https://www.novascotia.com/see-do/outdoor-activities/oakfield-provincial-park/1948)
is a very popular park located on Shubenacadie Grand, which features a small beach and picnic area.


### Shubie Park
If you are visiting Dartmouth Crossing and you want some fresh air, [Shubie Park](https://www.shubenacadiecanal.ca/) might be the place for you. Not only you can get some fresh air, but you also walk along the Shubenacadie Canal, and it cannot get better than this. Also, expect to see some wild animals as well!

### Belchers Marsh Park
[Belchers Marsh Park](https://www.halifaxtrails.ca/belchers-marsh/) is on Parkland Dr. A beautiful place to take a morning walk or walk dogs. A quiet place to do meditations and relax. It gives a feeling of getting out of the city.

### Westmount School Playground and Splash Pad
[Westmount School Playground and Splash Pad](https://www.chatterblock.com/resources/55007/westmount-school-splash-pad-halifax-ns/) is right across the street from the Halifax Shopping Centre, so easy to access by public transport or you could combine a trip to the mall with some time on the playground.

### Peace and Friendship Park
[Peace and friendship park](https://fathomstudio.ca/our-work/cornwallis-park) is located right in front of the Westin Hotel in the South End of Halifax. If you are looking for a garden to go for jogging, it is a suitable place.

### Oakland Road Park
[Oakland Road Park](https://goo.gl/maps/k2tNgauFYAE2WusW9) is a small park in Halifax's South End. It has a beautiful dock with pretty sunset and lake views; it is a nice and calm getaway from the city while it is about 5 mins of walk from campus.

### Victoria Park
[Victoria Park](https://goo.gl/maps/dME8zSf6S9FtQeHT9) is not big, but it is a good place to rest. Pigeons often gather under the statue, and there are fountains in the summer. It's a great place to take pictures.

### Frog Pond Park
[Frog Pond Park](https://scotiasites.com/frog-pond/) is a small park with a 1.4km trail surrounding a pond. This is a good place to walk around and enjoy the wildlife culminating at the pond, such as ducks, chipmunks, and frogs.


## Beaches

### Rainbow Haven Beach
A decent [beach nearby Halifax](https://www.novascotia.com/see-do/outdoor-activities/rainbow-haven-beach-provincial-park/2203)

### Dingle Beach
[Dingle Beach - Sir Sanford Fleming Park](https://www.novascotia.com/see-do/attractions/sir-sandford-fleming-park-the-dingle/1521) is a 95-acre park with four natural habitats (woodlands, heath barren, salt water and pond; please do not feed the waterfowl), walking trails, the Dingle Tower with bronze lions at the foot, a sandy beach (unsupervised swimming), a wharf and a boat launch.

### Carters Beach
[Carters Beach](https://www.sandylanevacations.com/activities.details.php?activity=carters-beach-nova-scotia) is a beautiful beach located in Port Mouton, Nova Scotia only a 2-hour drive from Halifax to the location worth the drive place is trash free water is transparent and crystal blue feels like you are on an island does not feel like Nova Scotia at all.

### Clam Harbour Beach Provincial Park
[Clam Harbour Beach Provincial Park](https://www.novascotia.com/see-do/outdoor-activities/clam-harbour-beach-provincial-park/1717) is a long white sand beach with a unique shallow tide stream that often means warmer water for swimmers.


### Lawrencetown Beach Provincial Park
It offers a long coastline. The weather here could be very windy, check before you go.

### Kinsmen First Lake Beach
[Kinsmen First Lake Beach](http://www.sackvillekinsmen.ca/) is located in Kinsmen Park, 71 First Lake Drive. People can play fireworks on New Year's Day or the Spring Festival on that beach with enough light around.

### Black Rock Beach
[Black Rock Beach](https://www.halifax.ca/parks-recreation/programs-activities/swimming/supervised-beaches-outdoor-pools-splash-pads) is at Point Pleasant Park on Sailors Memorial Way. Here is a tourist attraction in Halifax. We can walk around and feel the sea breeze.
[Google Map Link](https://goo.gl/maps/LEhaQpPJtXLaZBmM8)


### Chocolate Lake Beach
[Chocolate Lake Beach](https://www.google.com/maps/place/Chocolate+Lake/@44.6384459,-63.6259791,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a21eccc552311:0x8716c002b46e4423!8m2!3d44.6388728!4d-63.6236068) is an ideal beach resort to drive to with friends or family for a swim on a hot summer day, and it's only a 5-minute drive from the city center.

### Conrad's Beach
[Conrad's Beach](https://scotiasites.com/conrads-beach/) is a perfect place to watch the sunset with your friends. Or to take a nice walk, the water is a little brisk and usually only gets warm around August.

### Crystal Crescent Beach
[Crystal Crescent Beach](https://parks.novascotia.ca/park/crystal-crescent-beach) has three white-sand beaches near the mouth of Halifax Harbor. Great place to go on a sunny day.

### Queensland Beach
[Queensland Beach](https://www.google.com/maps/place/Queensland+Beach+Nova+Scotia/@44.6351658,-64.0438072,14z/data=!3m1!4b1!4m5!3m4!1s0x4b59ec17e9ca0f8b:0xa54749506702ad77!8m2!3d44.6351677!4d-64.0262976) is a bit further from the heart of the city, but well worth it. Make a trip to Queensland with family and friends on a hot summer day. This beach is supervised but becomes packed with people very quickly!


### Williams Lake Beach
[Williams Lake Beach](https://www.gov.mb.ca/sd/parks/park-maps-and-locations/western/william.html)
is east of the main Turtle Mounta in Provincial Park. It is well-known for its stocked waters, producing great catches of rainbow trout over the years.

## Tours
### Harbour Hopper
[Harbour Hopper](https://www.harbourhopper.com/) Located on the iconic waterfront, the 55-minute number #1 boat tour in Halifax operates between May->October.

### Tall Ship Silva
[Tall Ship Silva](https://www.ambassatours.com/experiences) This is a great option to get out on the Halifax harbor and see the city from a different perspective. You can also enjoy a beverage while you take in the sights and are shown the landmarks of Halifax.

## Sports & competitions <a name="sports"></a>
### Ski Wentworth
[Ski Wentworth](https://skiwentworth.ca/) This is a great ski hill located about an hour and a half north of Halifax. It is close to the town of Truro and offers many different types of trails and riding conditions.

### Ontree Park
[Ontree Park](https://www.ontreepark.com/) is a place to have outdoor activities in the woods.

### Ski Martock
[Ski Martock](https://www.martock.com/) It is my favorite place to go in winter; you can choose to ski or snowboard here; the experience is amazing!

### Emera Oval
[Emera Oval](https://www.halifax.ca/parks-recreation/programs-activities/outdoor-recreation/emera-oval) is a public outdoor skating surface. We can do in-line skating, cycling, and skateboarding in the summer and ice skating in the winter.

### Seven Bays Bouldering
[Seven Bays](https://sevenbaysbouldering.com) is an indoor bouldering rock climbing gym with an attached cafe and sitting area. Bouldering is the style of rock climbing where you don't attach yourself to the wall with a harness and only climb about 20 feet up. It has a chill vibe, and the rock climbing routes range in difficulty from beginner to expert.

### Halifax Wanderers Stadium/Grounds
[Halifax Wanderers Stadium/Grounds](https://hfxwanderersfc.canpl.ca/stadium-profile) Located in downtown Halifax, besides the Halifax Public Gardens, this small stadium is home to the Halifax Wanderers soccer team.

### East Coast Surf School
[East Coast Surf School](https://ecsurfschool.com) is a great way to get into surfing. They give daily surf lessons at Lawrencetown beach and offer rentals if you like to surf but don't have the gear. Surfing is a cool and fun summer activity to try with friends or family.

### Halifax Junior Bengal Lancers
[Halifax Junior Bengal Lancers](http://www.halifaxlancers.com/) is a non-profit horseback riding school with youth and adult programs. Located in the center of downtown Halifax, Lancers is a great place to visit to watch horseback riding right in the city.

## Hikes
### MacCormack's Beach Provincial Park
[MacCormacks Beach Provincial Park — a trial near fisherman's cove](https://parks.novascotia.ca/park/maccormacks-beach) is located on 1641 Shore Rd. It is not a long trail,  fitting for short walking and afternoon sunshine. The Atlantic Ocean is beside you,  while the seagulls are constantly flying over your head.

### Duncans Cove
[Duncans Cove](https://www.halifaxtrails.ca/duncans-cove/) is a seaside hike along the coastline just south of the Halifax harbor. It connects to two old bunkers and winds along the rocky shoreline for about 5km.

### Heart Rock
[Heart Rock / Collpitt Lake Trails](https://www.halifaxtrails.ca/)
Large Wilderness Park, Volunteer Run. These trails are mainly for Mountain Bikers.

### Sir Sandford Fleming Park Trail
[Sir Sandford Fleming Park Trail](https://www.halifaxtrails.ca/sir-sandford-fleming-park/) is a short hike that consists of a waterfront boardwalk and forested nature trails. The park also connects to the nearby frog pond trail.

### Chain of Lakes Trail
[Chain of Lakes Trail](https://www.halifaxtrails.ca/chain-of-lakes-trail/) is an easy 7.3 km trail passing by multiple lakes in and around Halifax. The trail begins on Joseph Howe Dr, across from the Atlantic Superstore, and ends on Lakeside Park Dr at the entrance of the Beechville Lakeside Timberlea trail.

### Hobsons Lake Trail
[Hobsons Lake Trail](https://www.google.com/maps/place/Hobsons+Lake+Trail/@44.683627,-63.7230048,14z/data=!4m13!1m7!3m6!1s0x4b598a67f4d1e3d3:0xf644dde18090792a!2sHobsons+Lake+Trail,+Nova+Scotia+B4B+1S8!3b1!8m2!3d44.6881066!4d-63.7055326!3m4!1s0x4b598a41d92a3f9f:0x87ada53da158abac!8m2!3d44.694922!4d-63.7047474) is about a 20-minute drive from downtown Halifax and is a great outdoor area with lots of rugged trails for adventure lovers to explore.

### Hemlock Ravine Park
[Hemlock Ravine Park](https://www.novascotia.com/see-do/trails/hemlock-ravine-park/6119) Located on Bedford highway, this park contains a unique heart-shaped pond along with a variety of trails surrounding it

### Polly's Cove
[Polly's Cove](https://www.halifaxtrails.ca/pollys-cove/) is a park with 3.9km of marked trails just 2km away from the world-famous Peggy's Cove.

### Long Lake
[Long Lake](https://www.halifaxtrails.ca/long-lake-provincial-park/) is a large provincial park with many trails surrounding Long Lake, only a 15-minute drive from downtown!

### Salt Marsh Trail
[Salt Marsh Trail](https://www.halifaxtrails.ca/salt-marsh-trail/) A trail that was previously a railway. About 9km long and is relatively flat.

### Shaw Wilderness Park
[Shaw Wilderness Park](https://www.natureconservancy.ca/en/where-we-work/nova-scotia/featured-projects/Shaw-wilderness-park.html) is a great hiking ground to explore the landscape of Nova Scotia. There is a short 3km trail which allows you to explore nature and see amazing views.

### Scotsbay
[Scotsbay](https://gitlab.com/daluniversity/explore-halifax.git) If you like hiking, Scotsbay is a place that I recommend to you; you can hike near the beach for about 4 hours long. There will be an amazing view at the end of the hiking trip.

### Admirals Cove
[Admirals Cove](https://www.halifaxtrails.ca/admiral-cove-park/) is a pretty easy hike in Bedford with some stunning views. It's most famous for "Eagles Rock" which is bolder on top of a cliff overlooking the Bedford Basin. It's a great place to hang out when no one else is around.

### Pollets Cove
[Pollets Cove](https://www.alltrails.com/trail/canada/nova-scotia/polletts-cove) Located on the Cabot trail. Heard many good things about this trail. Great View, and you can also spot wild horses as well.


## Gyms

### GoodLife Fitness
[Goodlife Fitness](https://www.yelp.ca/biz/goodlife-fitness-halifax-12) is one of my favorite places to go. It's at 5657 Spring Garden Rd. It has good fitness equipment, but it is not cheap if you need a coach.

### Dalplex
[Dalplex](https://athletics.dal.ca/facilities/Dalplex.html), situated just across the Studley Campus of Dalhousie University, is one of the more popular gyms in central Halifax. Dalplex consists of pools, courts, gym equipment, and much more but is often busy and requires membership for access.

### YMCA Health & Fitness
[YMCA HEALTH & FITNESS](https://www.ymca.ca/what-we-offer/health-and-fitness) there is only one location in Halifax for YMCA HEALTH & FITNESS. It is a mixed gym for males and females, open Wednesday-Friday from 5:45 AM- 10 PM, located at 5640 Sackville Street, Halifax NS B3J 1L2. It has 4.5/5 stars. Many people recommended it, and they say the staff is very friendly and nice feel welcoming.

### House of eights
[House of eights](https://houseofeights.com/class-schedule/)
a place to dance and relax, make friends with the same interests

### Canada Games Centre
[Canada Games Centre](https://canadagamescentre.ca/) is a sports and recreation center near Clayton Park in Halifax. The center was built for the 2011 Canada Games in Halifax. It is now a recreational facility, offering a pool with several aquatics programs, a running track, and a gym. One of the most impressive things about the building has to be the water slide which starts inside the building, and travels outside for a few loops before re-entering the building proper

### Fit4Less
[Fit4Less](https://www.fit4less.ca/) has multiple locations around the HRM. An affordable alternative to Goodlife starting at just 6.99 every two weeks.

### 30 Minute Hit
[30 Minute Hit](https://www.30minutehit.com/) Great gym location, especially for those that enjoy boxing. You could get an amazing workout here, plus great staff.

### Impact Fitness
[Impact Fitness](https://impactfitness.club/) Small but a great gym, comes with a variety of weights and equipment, staff are friendly most of the time :)

### Queensbury Rules Boxing
[Queensbury Rules Boxing](https://www.qrulesboxing.com/) is a gym with various classes and an option for sparring for boxers at every level. Trials start at $25 + tax for two weeks, and a student discount is also offered when purchasing a membership.

### Titans MMA
[Titans MMA](https://duckduckgo.com/?q=titans+mma&atb=v318-1&ia=places&iai=7383383400122142720&iaxm=places) is the best MMA gym in Halifax. They host a variety of classes for athletes of different levels in MMA, BJJ, boxing, and kickboxing.

### JBTC - Nova Scotia Junior Badminton Training Center
[JBTC - Nova Scotia Junior Badminton Training Center](https://www.nsjbtc.com/) 10,000 sqft of professional facilities dedicated to badminton training and competition. The address is 200 Bluewater Rd
Bedford NS, B4B1G9.

## Live Music <a name="music"></a>
### Durty Nelly's
[Durty Nelly's](https://durtynellys.ca/) is a classic Irish pub with live music seven nights a week! With a great selection of food & drinks, it is a must-visit place in Halifax.

### The Old Triangle Irish Alehouse
[The Old Triangle Irish Alehouse](https://www.oldtriangle.com/welcome/) The Celtic Heart of the Maritimes! Experience the best of Irish cuisine and drink Halifax has to offer here on 5136 Prince St. Enjoy some of the greatest live music Nova Scotia artists have!

### Split Crow Pub
[Split Crow Pub](https://www.splitcrow.com) The Split Crow Pub is proud to be 'Nova Scotia's Original Tavern' and continues to serve locals and travelers worldwide. A welcoming smile, generous mugs of grog, fantastic food, and, of course, toe-tapping music welcomes you at the Split Crow Pub. Great live music and $2.50 beers during power hour!

### Dalhousie Arts Centre
[Dalhousie Arts Centre](https://www.dal.ca/dept/arts-centre.html)
The Dalhousie Arts Centre hosts shows/concerts that are truly breathtaking. I've been lucky enough to go to a show by the legendary guitarist Tommy Emmanuel and a show by the Symphony Nova Scotia playing pieces by my favorite classical composer, Maurice Ravel. They also do other forms of live media.

## Entertainment
### Captured Escape Rooms
![Captured Escape Rooms](images/escape_room.png)
[Image credit](https://capturedescaperooms.com/rooms/plunder)

[Captured Escape Rooms](https://capturedescaperooms.com/) are designed for intense participation from the group trying to escape. It's perfect for team building experience!

### Bára Whitewater Rafting
[Bára Whitewater Rafting](https://www.barawhitewater.com/) is the closest rafting location to Halifax for a particularly exciting experience.

### Red Bar
[Red Bar](http://www.redbar.ca/) is a karaoke located at 5680 Spring Garden Rd. You can not only show your beautiful voice there but also enjoy some live music by various bands.

### Trapped Halifax
[Trapped Halifax](https://trapped.com/locations/halifax.html) is a similar establishment located on Barrington St, offering a variety of escape rooms for groups to attempt.

### Propeller Arcade
[Propeller Arcade](https://drinkpropeller.ca/pages/propeller-arcade) A retro arcade with many pinball machines, ski balls, and other arcade cabinets located in the lower level of Propeller Brewing Company's Gottingen Street taproom.

### Scotiabank Theatre Halifax
[Scotiabank Theatre Halifax](https://www.cineplex.com/Theatre/scotiabank-theatre-halifax) is the only theatre with IMAX in Halifax; it has comfortable chairs and excellent quality visuals and sounds.

### Yuk Yuk's
[Yuk Yuk's](https://www.yukyuks.com/) is a local comedy club and an excellent place to go for a laugh!

### Nova Tactical
[Nova Tactical](https://www.novatactical.ca/) is a good place to experience the joy of shooting. The service includes archery ranges, airsoft ranges, and gun ranges; you also can buy firearms and fishing tools.

### Halifax Ghost Walk
[Halifax Ghost Walk](https://www.novascotia.com/see-do/tours/halifax-ghost-walk/7926) Experience the haunted history of Halifax by attending the Halifax Ghost Walk tour located at The Old Town Clock on Brunswick Street. Learn about the thrilling events of Halifax's chilling run-ins with pirates, ghost tales, and eerie happenings on this journey through the city!


### Get Air Trampoline Park
[Get Air Trampoline Park](https://getairsports.com/nova-scotia/) is a great place for kids and adults; cheap and very interesting. Great place to have activities indoors when the weather isn't sunny.

### The Comedy Cove
[The Comedy Cove](https://thecomedy-cove.com/) local stand-up artists trying to make you laugh. Sometimes good, sometimes bad, but fair warning – don't come if you are easily offended.


### Mersey Road Paintball
[Mersey Road Paintball](https://www.facebook.com/Mersey-Road-Paintball-301437796536617/) is a great opportunity to enjoy simulated combat for sport. The field offers walk-on options for new players where they can rent out gear and purchase paintballs on site. Mersey Road contains a mix of wooded areas and civilian-style structures.

### The Deck Box
[The Deck Box](https://www.thedeckboxhalifax.com/) is a game store for trading card games, board games, and video games, including but not limited to Magic the Gathering, Pokemon, Yu-Gi-oh, Dungeons, and Dragons, etc. Deck Box also provides a place and space to play card games and air conditioning, which is a good place for trading card game lovers to meet and communicate offline.

### Neptune Theatre
[Neptune Theatre](https://www.neptunetheatre.com) is a wonderful theatre located on Argyle Street. Neptune is a great place to watch live performances such as plays, musicals, and concerts.

### Scotiabank Centre
[The Scotiabank Centre](https://www.scotiabank-centre.com/) is the largest facility in Atlantic Canada. Home to the hockey team, the Halifax Mooseheads, and the Halifax Thunderbirds lacrosse team, this multi-purpose arena hosts sports games, concerts, and restaurants.

### Bowlarama
[Bowlarama](https://southcentrebowlarama.ca/en) is a fun place to visit with family and friends. It contains an arcade, bowling alleys, a bar, and food and drinks, so you can easily spend a whole day.

### Playdium
[Playdium](https://www.playdium.com/) is a fun place with a vast assortment of arcade-style games, ride-on, VR games, and bowling lanes. It also has an in-house restaurant which is pretty decent—an excellent place to visit over the weekend with family and friends.

### Casino Nova Scotia
[Casino Nova Scotia](https://www.google.ca/maps/place/Casino+Nova+Scotia/@44.6527542,-63.5781077,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a23cdfb7fa64f:0x809350b9ce6629ec!8m2!3d44.6526505!4d-63.5760523?hl=en&authuser=0) is the most famous casino in NS province and the staff are very friendly. And they serve a very delicious buffet. The environment in that place is awesome, and it is also very close to the sea.

### Parklane Theater
[Parklane Theater](https://www.cineplex.com/Theatre/cineplex-cinemas-park-lane)
Parklane theater is located at 5657 Spring Garden Road. You can watch screenings of the latest Hollywood films, plus new independent releases.

### Halifax Wanderers
[Halifax Wanderers](https://hfxwanderersfc.canpl.ca/) is a football/soccer team that takes part in the Canadian Premier League, and the supporters come and watch
the match to support their team and have a good time


## Events
[Baisakhi- in Halifax](https://www.eventbrite.ca/e/baisakhi-in-halifax-tickets-312234320257) is one of the events organized by Utsav at Waterfront every year. It is a lot of fun with music, different Indian cuisines, and fun-loving folks!

### Halifax Burger Bash
[Halifax Burger Bash](https://burgerbash.ca/) takes place every year from April 28 to May 7. During that time, Halifax restaurants will sell their creative burgers (more than 130, priced at $7 or more) and donate the proceeds from each burger to [Feed Nova Scotia](https://www.feednovascotia.ca/). If you're interested in burgers, don't miss it.

### East Coast Kite Festival
[East Coast Kite Festival](https://www.familyfuncanada.com/halifax/east-coast-kite-festival/) is a non-alcoholic, family and kids-friendly event. You can come and enjoy your weekend with kites at Citadel Hill. Their team has over 20 new big kites to display, including new 3D kites!

### The Busker Festival
[The Busker Festival](https://www.buskers.ca) is an annual festival in Halifax showcasing the many diverse talents of busking performers from around the world. The festival occurs in the summer and is a wonderful way to spend some time on the Halifax Waterfront.

### Halifax Jazz Festival
[Halifax Jazz Festival](http://www.halifaxjazzfestival.ca/)
Designated a Hallmark Event by the Halifax Regional Municipality, the Halifax Jazz Festival attracts up to 65,000 visitors, involves 400 volunteers, and employs over 350 local musicians. The Halifax Jazz Festival will take place July 13-17, 2022.

### Halifax Color Festival
[Halifax Color Festival](https://allevents.in/halifax/halifax-colour-festival-2022/200022309100406) is an annual event where people dance and play with colors. Food trucks are usually available if you want to grab something to eat. It is open to people from all ages.

## Night Clubs
### The Dome
[The Dome](https://www.thedome.ca/) Known by many as "Home", the Dome is a staple nightclub in downtown Halifax. A must-see for any dance enthusiast or audiophile looking for a thoroughly mediocre experience.

### The Seahorse Tavern
[The Seahorse Tavern](https://www.facebook.com/TheSeahorseTavern/) is a hotspot for live music and some of the best parties in the city.

### Pacifico
[Pacifico](https://www.pacificohalifax.com/) Located on George Street in the heart of downtown, Pacifico is famous among locals and visitors for its incredible live Jazz nights on Thursdays.

### Midtown-Boomers Tavern
[Midtown-Boomers Tavern](http://www.themidtown.ca) The Midtown Tavern & Lounge is located in downtown Halifax, Nova Scotia, directly across from the Dome / Cheers nightclub. Offering late-night beverages and drink specials daily.

### The Den
[The Den](https://www.thedenhfx.ca) The Den is a nightclub with arguably one of the best Music in Halifax. With a great atmosphere and diverse group of people, it is one of the best places to be on a Friday night.

### Hide and Seek
[Hide and Seek](https://www.hideseekhfx.com) Hide and seek is a nightclub in downtown Halifax. It has a calm and chill atmosphere with banging beats and is also connected to the Loft nightclub, making it so that you pay one price for two nightclubs.

## Restaurants
### Indian
#### Rasa
[Rasa:Flavours-of-India](https://www.google.com/maps/place/Rasa:+Flavours+of+India/@44.6439747,- 63.5797514,17z/data=!3m2!4b1!5s0x4b5a222dd3438d0f:0xd20be503d47b3cee!4m5!3m4!1s0x4b5a23004eef13eb:0 xd1d54719c56cd478!8m2!3d44.6439747!4d-63.5775627)
Rasa is a fine dining Indian cuisine restaurant. It is famous for its affordable choices in the main course with no compromise in providing authentic Indian flavors.

## Adda Indian Eatery
[Adda Indian Eatery](https://www.addaindianeatery.com/menus) is located in Spring Garden Place. This restaurant is one of the best Indian restaurants with the vibes of India and food to offer from all parts of India. If you feel away from home as an Indian, this is a place you shall surely go to and eat food.

#### Masala Delight
[Masala Delight](https://www.masaladelight.com/) It's a great place for Indian cuisine, especially south Indian. They have one of the best dosas in Halifax.

#### Tawa Grill
[Tawa Grill](https://176838.com/tawa) makes classic Indian cuisine with great portions and taste for reasonable pricing. Its ambiance makes for a very romantic date spot after evening, and the staff is super nice. They had a daytime lunch buffet for $14, but they, unfortunately, don't have it anymore.

### Chinese
#### New Panda Buffet
[New Panda Buffet](https://www.google.com/maps/place/New+Panda+Buffet/@44.6802511,-63.5434644,17z/data=!3m2!4b1!5s0x4b5a2409aaa26bb3:0xdc4104961d081df7!4m5!3m4!1s0x4b5a2578f87518ed:0x494ac8e805c9c0a!8m2!3d44.6802473!4d-63.5412757) It's a buffet restaurant with Chinese dishes, Japanese dishes and some Korean dishes. The music in this restaurant is also very unique, all of which is Classical Chinese tunes. This restaurant is cheap; you can eat all you want for $15.

#### 9+nine
[9+nine](https://www.9plus9.ca/) is a place to have authentic Chinese food.

#### Fan's Chinese Restaurant
[Fan's Chinese Restaurant](http://www.fansrestaurant.com/) A Chinese restaurant on Windmill Road in Dartmouth serving dim sum, hotpot, and other dishes. The best dim sum I've had in HRM with lots of selection.

#### Xiaoyu Homestyle Noodle
[Xiaoyu Homestyle Noodle](https://goo.gl/maps/Mx5zb61Q39ZisTGV7) is an authentic Chinese restaurant. You can get some real Chinese-style noodles here. There is some spicy food.

#### Happy Veal Hot Pot
[Happy Veal Hot Pot](https://happyvealhotpot.com/) This is an excellent Chinese restaurant; I often order their food on the takeaway platform. The taste is also very fragrant. The price is also very good.

#### Shanxi Paomo
[Shanxi Paomo](https://shanxipaomo.com/) This is a great Chinese restaurant in Quinpool, the Roujiamo (kind of like a Chinese burger) and Liangpi (something like cold rice noodles) taste amazing, and the price is not high!


#### Jean's Chinese restaurant
[Jean's Chinese restaurant](https://www.jeansrestaurant.ca/) has many delicious Chinese food items; you can order food on the official website or go to the store to taste it at 5972 Spring Garden Rd.

#### Friend's Chinese
[Friend's Chinese restaurants](https://www.google.ca/maps/place/Friend's+Chinese+restaurants/@44.6378966,-63.5757484,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a2237c85cfda7:0xce20274e7dcee6e8!8m2!3d44.6379111!4d-63.573569?hl=en) is a Chinese restaurant which is populator in Chinese students. This restaurant also offers takeaway service.

#### Qiu Brothers Dumplings
[Qiu Brothers Dumplings](https://qiubrothersdumplings.com/) is at 1335 Barring Street. Same as the restaurant name, there have some delicious Chinese-style Dumplings. Otherwise, there also have some other Chinese food.
[Google Map](https://goo.gl/maps/5BbUgWvko2GJfkwn9)

### Mediterranean
#### Mezza Lacewood
This [Mezza](https://www.mezzalebanesekitchen.com/canada/) is the busiest among the Mezza in Halifax. It is at the hotspot of lacewood beside many other food stores. It is also Halal, which draws a lot of attention from Muslim customers.

#### Bakeaway
[Bakeaway](https://www.bakeaway.ca/) Bakeaway is a Middle-Eastern cuisine that contains a lot of the most famous foods in the Arab world. It makes the best Middle-Eastern dishes in Halifax. They offer dishes such as
Falafel wraps, Shawarma, Vine leaves.

#### 902 restaurant & catering
[902 restaurant & catering](https://www.902halifax.com/media) 902 restaurant & catering is a Middle Eastern restaurant that makes Middle-Eastern Lunch and dinner meals such as Kebabs, Lamb Shank, Roasted chicken, Traditional Dishes, Fried Chicken, Shawarma, Hummus, Wraps, Burgers, it is located at spring garden at 1579 Dresden Row, Halifax, Nova Scotia B3J 2K4

#### Chef Abod Cafe & Catering
[Chef Abod Cafe & Catering](https://www.chefabod.com/) is a Middle Eastern restaurant known for various dishes, including their Kabsa, Mandi, and Shawarma dishes. They are located on 3217 Kempt Road and offer Dine-in, Delivery, Pick-Up, and catering services.

### Japnese and Korean <a name="jap_korean"></a>
#### Wasabi House
[Their](https://wasabihouse.ca/wasabi-house-halifax/) sushi is delicious, and they often give customers extra snacks. Also, I remember they seem to have discounts after 9.00 PM.

#### Song's restaurant
[Song's restaurant](http://www.songskoreanrestaurant.com/) is a small Korean restaurant with a nice atmosphere and good food.

#### JUKAI Japanese & Thai
[JUKAI Japanese & Thai](https://jukaidartmouth.com/), all you can eat for 29.99 for each person. They have the best sushi in HRM.


#### Sushi Nami Royale
[Sushi Nami Royale](https://sushinami.ca/) It is one of the best sushi restaurants in the city; everything is fresh.

#### Busan Korean BBQ
[Busan Korean BBQ](http://busankoreanbbq.ca/) is a great Korean barbeque place to go out with family or friends. It has a special lunch menu from 12:00–4:00 PM for affordable and delicious Korean dishes.

#### Sushi Jet
[Sushi Jet](https://goo.gl/maps/VskYQxuoTvEWfHDq8) is a very convenient and affordable Japanese restaurant. I especially like their "Sushi Appetizer."

#### Kor-B-Q
[Kor-B-Q](https://korbq.ca/)  is a Korean BBQ restaurant located at 278 Lacewood Dr. Halifax. This Korean Grill restaurant serves All-you-can-eat Korean BBQ. The food there tastes very good, they have fresh ingredients and premium raw meats, and the price is not very high.

#### Sushi Cove
[Sushi Cove](https://sushicovehalifax.com/) is a lovely family-run business with beautifully presented and fresh sushi with original roll combos and the crispiest tempura!

#### Gangnam Korean BBQ Halifax
[Gangnam Korean BBQ Halifax](http://gangnamkoreanbbq.ca/) is the best Korean restaurant. It's at 1261 Barrington St. it has the best Korean BBQ cuisine. The ingredients are very fresh, and the service is very friendly.

#### Bap House Korean Kitchen
[Bap House Korean Kitchen](https://www.baphouse.ca) is a Korean restaurant located on Quinpool Road. It serves healthy and fresh food. You can also create your bibimbap!


### Italian
#### La Frasca Cibi & Vini
[La Frasca Cibi & Vini](https://lafrasca.ca) A beautifully designed Italian restaurant located in the downtown area at 5650 Spring Garden Rd combines classic Italian dishes with a modern twist. A lovely ambiance creates a perfect location for a date night or anniversary meal!

#### Mappatura
[Mappatura](https://mappaturabistro.ca/) - Italian restaurant on Spring Garden Road.

### Formal
#### Muir Restaurant
[Muir, Drift, Cafe Lunette](https://muirhotel.com/taste/) are part of Muir hotels and restaurants. Muir is one of few restaurants mentioned in Forbes and is perfect for formal gatherings and events. Worth trying.

#### aFrite
[aFrite](https://afrite.ca/) is a restaurant located at [1360 Lower Water St](https://g.page/Afriteresto?share) owned by Masterchef Canada contestant (Seasons 2 and 7) Andrew Al-Khouri. aFrite showcases modern Mediterranean fusion based on Andrew's mother's traditional Syrian cooking style.

![aFrite](images/afrite.jpg)<br/>
[Image credit](https://afrite.ca/about/)


### Fast food
#### Spring Garden McDonald's
The [McDonald's on Spring Garden road](https://www.mcdonalds.com/ca/en-ca/location/nova-scotia/halifax/spring-garden-rd/5675-spring-garden-road-ste-g08/29712.html) is often quite busy, but it is a great place to go for your 3 AM McNugget cravings. This is easy to navigate for Dalhousie students, as it is a straight line east from Coburg Rd.

#### Quinpool Road McDonald's
A [McDonalds](https://www.mcdonalds.com/ca/en-ca/location/halifax/halifax-quinpool/6324-quinpool-road-/5652.html) restaurant located on Quinpool road. It offers a drive-thru and a mural of the public gardens.

#### Charger Burger
[Charger Burger](https://www.chargerburger.com/) is one of the few Halal burger restaurants in Halifax. They use fresh ingredients from top-quality Nova Scotia producers. They have a great dining area with friendly staff.

#### Timberlea Tavern
[Timberlea Tavern](https://www.facebook.com/thetbr/) is a great place to enjoy bar foods such as wings, burgers, nachos, sandwiches, salads, and much more while enjoying a few drinks. Be sure to check their event schedule for live performers.

#### Chkn Chop
[Chkn Chop](https://www.chknchop.com), as the name implies, is a chicken-oriented restaurant in the north end of Halifax. They serve a variety of sandwiches, poutine, cooked chicken, drinks, and more. They also have weekly rotations on their sandwiches and wings every Tuesday.

#### The Chickenburger
[The Chickenburger](https://chickenburger.com/) is a staple of Bedford, a classic dinner/fast food restaurant located on the Bedford Highway. They've been open since 1940. They sell chicken burgers, hamburgers, hot dogs, fish and chips, and more. I always like getting their milkshakes.

#### KFC
[KFC](https://www.kfc.ca/menu/overview) is the only KFC in downtown Halifax. If you want the familiar KFC taste in Halifax, go to this one.

#### Fries&Co
[Fries&Co](https://friesnco.com/menu.html) Located at 2603 Connolly St. It mainly sells fries and fried fish. It is one of the highest-rating fried fish stores in Halifax.

#### Woody’s BBQ
[Woody's BBQ](https://www.yellowpages.ca/bus/Nova-Scotia/Dartmouth/Woody-s-Bar-Bq/7933688.html) Their fried corn-on-the-cob with garlic butter is a delicious and unique side. They are open nightly all week. The address is 159 Hector Gate, Dartmouth, NS B3B 0E5.


### Others

#### Swiss Chalet
[Swiss Chalet](https://www.swisschalet.com/)is a Canadian chain of casual dining restaurants. The roast pork ribs are delicious.

#### Café Aroma Latino
[Café Aroma Latino](https://www.cafearomalatino.com/) is a great spot to try Latin American cuisine; various dishes from breakfast to lunch can be enjoyed with region-specific drinks. In addition to serving food, you can purchase some grocery items from these countries.

#### Boondocks
[Boondocks](https://www.boondocksrestaurant.ca/) is a classic seafood restaurant on the shore of the inlet, facing McNabs island. The seafood chowder and the fish pie are my favorites. Just ask to add salt (They don't put enough).

#### Shiraz Restaurant
![Shiraz Restaurant](images/Shiraz.JPG)

[Image credit](https://www.tripadvisor.ca/Restaurant_Review-g154976-d790152-Reviews-Shiraz-Halifax_Halifax_Regional_Municipality_Nova_Scotia.html#photos;aggregationId=101&albumid=101&filter=7&ff=75159063)
[Shiraz](https://www.tripadvisor.ca/Restaurant_Review-g154976-d790152-Reviews-Shiraz-Halifax_Halifax_Regional_Municipality_Nova_Scotia.html) is famous for its delicious Persian cuisine. If you are a Kebab fan, Shiraz should be one of your stops.

#### Battery Park Bar and Eatery
![Battery Park Bar](images/bar.jpg)
[Image credit](https://batterypark.ca/)

There is a lot of exciting food that can be found in Halifax. Multiple cuisines, including Indian, Korean, Japanese, and French, can be found.
The best place to find cheap and interesting Nachos is Battery Park Beer Bar and Eatery in Dartmouth. The menu and the further details of the bar can be accessed from their [website](https://batterypark.ca/).
They serve till 11:00 PM. However, it is open till midnight. It is on a first come and first serve basis; hence, no reservations are required.

#### Darrell’s
[Darrell's](https://darrellsrestaurants.com/) restaurant is a great diner-type place that serves award-winning burgers and shakes. My go-to dish is their award-winning peanut butter burger.

#### Feasts
[Feasts](https://www.thefeasts.ca/) All dishes are made with delicious fresh ingredients right before you! It can be customized as per your preferences.


#### Casablanca
[Casablanca](https://casablancahfx.ca/) is an amazing restaurant with a great interior that offers delicious and authentic Moroccan food in Halifax.

#### 5 Fisherman
[5 Fisherman restaurant](https://www.fivefishermen.com/) is a place that only provides seafood.

#### The Foggy Goggle
[The Foggy Goggle](https://www.thefoggygoggle.ca/) The Foggy Goggle serves up a delicious assortment of healthy pub fare classics with a twist. Vegan and gluten-free options are available.

#### The Keg
[The Keg](https://kegsteakhouse.com/en/locations/halifax) is a good steak house. They have delicious food, and it is a really perfect place to date. I bet this is the best steak house in Halifax.

#### Buta Ramen
[Buta Ramen](https://www.butaramen.ca) is a good place if you're craving some ramen. They've got other appetizers, too, like edamame.

#### The Board Room Cafe
[The Board Room Cafe](https://theboardroomgamecafe.mybigcommerce.com/) is a board game cafe located on Barrington St. There's a wide variety of games you can play with friends for only $6 per session, plus a menu of snacks, meals, and drinks to keep you going!

#### Boston Pizza
[Boston Pizza](https://bostonpizza.com/en/locations/downtown-halifax.html) Boston Pizza, located on Granville Street, has a variety of delicious pizzas and fries, as well as a wide selection of drinks. The clean environment inside the hotel makes you feel comfortable. Customers can customize the pizza they want according to their preferences,
one of the best pizza places in Halifax downtown.

#### Sea Smoke Restaurant & Bar
[Sea Smoke Restaurant & Bar](https://www.seasmokehalifax.com) is a fantastic seafood restaurant on the waterfront with a beautiful patio and fire pit. They have nice oysters and sushi too.

#### Shadia's Pizza
[Shadia's Pizza](https://shadias-pizza.com) is the best place to get pizza, not only has pizza but also burgers and proteins. It is highly recommended. Fast delivery and delicious food!.

#### Cha Baa Thai Restaurant
[Cha Baa Thai Restaurant](https://chabaathairestaurant.ca)  is a restaurant that serves authentic Thai dishes and is recommended to people who like food with a spicier flavoring to satisfy their palates. Their Red Curry Noodle Soup and Pad Thai are especially delicious.

#### Mary's Place Cafe II
[Mary's cafe](https://www.facebook.com/marysplacecafe2/) is a very nice diner close to Dalhousie University and thus is a perfect place for students to visit. The food at the Mary's is delicious and well priced and contains many different types of food in each dish. The people working there are very friendly. location on [map](https://g.page/marysplacecafe2?share).

#### Armview Restaurant & Lounge
[Armview Restaurant & Lounge](http://www.thearmview.com/) is an old-school diner that overlooks the arm. They serve breakfast, sandwiches, steaks, and burgers.

#### Antojo Tacos & Tequila
[Antojo Tacos & Tequila](https://antojo.ca/) serves vibrant, Mexican-inspired dishes, plus an impressive collection of tequila and mezcal — all stunningly designed, eclectic settings.



#### Edna
[Edna](https://www.ednarestaurant.com/) is a bustling one-room restaurant with a comfy bar. They have a neighborhood bistro serving locally sourced and seasonally inspired dishes.

#### CUT Steakhouse
[CUT Steakhouse](https://www.rcr.ca/restaurants/cut-steakhouse/) is one of the most famous steak restaurants in town. Also, this restaurant has dry-aged steak, where you can enjoy the different steak flavors in Halifax.

#### The Red Asian Fusion Restaurant
[The Red Asian Fusion Restaurant](https://goo.gl/maps/A6ACrbmphh6cgqmX7) This Chinese restaurant is my frequent go-to. Every dish of theirs is delicious and large. The Farmhouse's Fried Pork is my most recommended dish.


#### Halifax Bread Factory
![Halifax Bread Factory](images/bread.jpeg)

[Image credit](https://halifax-bread-factory.business.site/)

[Halifax Bread Factory](https://halifaxbread.ca/) is the only place in Halifax that bake different variety of Persian (Iranian) breads and foods. Very clean and organized place. I am sure you will love this place.

#### Yonghe Noodle House
[Yonghe Noodle House](https://goo.gl/maps/wpP3QUkgGCHoF7uk6) This noodle restaurant is my favorite, and I like their curry chicken noodles the most.

#### Tony's
[Tony's](https://www.tonysdonair.ca/) is a local donair and pizza shop (family business) in Halifax, opened in 1976. Their TONY's FAMOUS DONAIRS is highly recommended. It is almost full of meat inside; not even the pita bread on the outside could wrap it completely (no matter the size).

## Mary's African Cuisine
[Mary's African Cuisine](https://www.marysafricancuisine.com) is an authentic African restaurant located on 1701 Barrington street. The restaurant offers a variety of foods from all over Africa. It is the perfect place to visit those who are homesick or looking to try some African food.

#### Your Father's Moustache
[Your Father's Moustache](http://yourfathersmoustache.ca/), established in 1989, is a family-owned pub. They offer live music as well as outdoor seating. The chicken wings are very good!


#### Kai Brady's Fancy Dive Bar
[Kai Brady's Fancy Dive Bar](http://kaibradys.ca/) This is a worthwhile meal bar downtown. They provide rich locally brewed beer and very exquisite food. (Tips: all hamburgers are half price every Wednesday!)

#### Montana's Cookhouse
[Montana's Cookhouse](https://www.montanas.ca/?gclid=Cj0KCQjwg_iTBhDrARIsAD3Ib5hlkX5J4RYg_7P3v83IHwtk8NasvCZIWuiOf74cmNxa7cogz2qHnugaAi20EALw_wcB) It's a chain restaurant, and their ribs and steaks are delicious. At the same time, the environment and service quality of the store is also very good, let people like it very much.

#### 360 Lounge
[360 Lounge](https://www.360loungehfx.com/) This is a fusion American restaurant that boasts various mouth-watering items on its menu. The best part is all the food is halal, so it offers options for people of all faiths.

#### The Wooden Monkey
[The Wooden Monkey](https://www.thewoodenmonkey.ca/)
is a really good restaurant, especially for seafood. It has two branches, one in Halifax and one in Dartmouth. They use the locally grown organic product.

#### Lot Six
[Lot Six](http://lotsix.ca/) is a yummy place to eat, and they have a bottomless mimosa brunch on the weekend. It's also a nice date night spot.

#### Eliot and Vine
[Eliot and Vine](https://eliotandvine.com/) Located on Clifton street, Eliot and Vine serve various amazing dishes in a cozy yet fancy environment.

## Coffee and tea <a name="coffee"></a>
### Golden Bakery
[Golden Bakery](https://www.goldenbakery.ca/)
Golden bakery is a nice local coffee shop located on Bedford highway.

### Wired Monk Bistro
[Wired Monk Bistro](https://www.facebook.com/wiredmonkhalifax) is a small local coffee shop and bistro located downtown Halifax.

### Uncommon Grounds
[Uncommon Grounds](https://theuncommongroup.com/pages/uncommon-grounds) is a lovely little coffee shop that is a great place to get out of the apartment to study with a warm drink and great food.

### Coburg Social
[Coburg Social](https://coburgsocial.com/) is a great cafe near Dalhousie's campus. They have a great selection of food and drinks that can be enjoyed inside or on their patio. This cute cafe also sells fun cocktails and other alcohol and, on occasion, hosts live music events.

### Cabin Coffee
[Cabin Coffee](https://cabincoffeehalifax.com/) Cabin Coffee is one of the oldest family-owned coffee shops in downtown Halifax.

### Good Luck Cafe]
[Good Luck Cafe](http://www.manualfoodanddrinkco.com/goodluck) Located at 145 Portland St in downtown Dartmouth, this cute cafe has everything from coffee and pastries to fresh produce and take-home meals.

### Tsujiri
[Tsujiri](https://www.tsujiri.ca/) A cafe and dessert shop located in The Curve on South Park Street. They specialize in matcha and sell drinks, sweets, and snacks inspired by traditional Japanese flavors.

### The Daily Grind Cafe & Bar
[The Daily Grind Cafe & Bar](https://thedailygrindcafebar.ca/) is located near the waterfront and is a great place to study or catch up with friends. As the name mentions, it is not only a cafe but a bar, which gives you the option to have your favorite beer while enjoying a good book.

### Coffeeology
[Coffeeology](https://99hwany.wixsite.com/coffeeologyespresso/) is a cafe located on Dresden Row. It is a tiny, cozy space known for its laid-back music and friendly environment. The coffee is amazing, and the baked goods are fresh from a local bakery.

### Narrow Espresso
[Narrow Espresso](https://g.page/narrowespresso?share) If you live near Fenwick street, this is the perfect place for your first cup of coffee in the morning. They open very early and have a rich variety of coffee and very favorable prices.

### Trident Booksellers & Café
[Trident Booksellers & Café](https://goo.gl/maps/5nAeWcJh9fCTDA9h7) is a old-fashioned cafe with a relaxed ambiance. However, most books might not be relevant, but their cafe is interesting as they make their coffee and have a decent variety of items in their bakery.

### Pane ē Circo
[Pane e Circo](https://www.panecirco.ca) is Halifax's newest Italian Salumeria, serving up highest quality products to-go, including a vast selection of imported cured meats & cheeses, sourdough bread & artisanal pastries, and even the fresh handmade pasta & sauces. If you get a chance to try it, I will get the almond croissant or any of the sandwiches!

### Creamy Rainbow Bakery and Cafe
[Creamy Rainbow Bakery and Cafe](https://g.page/creamy-rainbow-bakery-and-cafe?share) is a very good coffee shop. The egg tarts and puffs are very delicious.

### Rabbit Hole Cafe
[Rabbit Hole Cafe](https://www.rabbitholecafe.ca/) An elegant Cafe & Dessert Bar located in the heart of downtown Halifax, 1452 Dresden Row. A great rest stop to have a quiet study session or your personalized cake designed to your liking.

### Dilly Dally Coffee Cafe
[Dilly Dally Coffee Cafe](https://my-site-106684-102240.square.site) is a coffee shop on Quinpool Road. You can grab a coffee or some amazing baked goods here! It provides a chill atmosphere with cute decorations.

### Glitter Bean Cafe
[Glitter Bean Cafe](https://www.glitterbeancafe.com) is a coffee shop on Spring Garden Road. It often has a seasonal drink on the menu and a lot of seating for anyone looking to study.

### The Kiwi Cafe
[The Kiwi Cafe](https://www.thekiwicafe.com)
is in Chester, the south shore of nova scotia, a beautiful place to have coffee and international themes. They are located right next to the sea, which is quite nice if you are a beach or sea person. You must visit.

### LF Bakery
[LF Bakery](http://lfbakeryhalifax.com/) is a yummy bakery with a bunch of authentic French pastries on Gottingen street in the North end.

### The Bread Lounge Bakery
[The Bread Lounge](https://thebreadloungebakery.square.site/) is a bakery and coffee shop located in downtown Halifax on Demone Street. They have pastries, desserts, sandwiches, soup, coffee, and more. A great place to study, meet with friends, or grab a quick lunch or snack!


### Chatime
[Chatime](http://www.chatimeatlantic.ca/) is located on 1480 Brenton Street. It's a cool shop for getting bubble tea, and it has a lot of variety in teas.

### Zenq
[Zenq](https://zenqhalifax.com/) is located on 1065 Barrington St. It serves Taiwanese Bubble Tea and Dessert.

### Hitea
[Hitea](https://www.google.com/maps/place/Hi+Tea/@44.6713508,-63.6765064,15z/data=!4m5!3m4!1s0x0:0x32f9fe93fde15b3!8m2!3d44.6713508!4d-63.6765064) Hitea is a really good bubble tea shop and it is located at 480 Parkland Dr, Halifax.

## Wineries and bars <a name="bars"></a>

#### Good Robot
![picture of Good Robot patio, with customers drinking in the sun](images/good-robot-image.jpeg)
[Image credit](https://bikefriendlyns.ca/node/111)

[Good Robot](https://goodrobotbrewing.ca/) is a restaurant and bar in the North End of Halifax, started by 3 engineers who were bored of the monotony of their jobs. They came together to create a funky robot themed establishment with a kickin outdoor patio lovingly called the GastroTurf as it is fully astroturf grass.

### Benjamin Bridge
![picture of the Benjamin Bridge patio at sunrise](images/Benjamin-Bridge-at-Sunrise.jpeg) [Image credit](https://winesofnovascotia.ca/portfolio/benjamin-bridge/)

[Benjamin Bridge](https://benjaminbridge.com/) is an award-winning vineyard located in the heart of Gaspereau Valley, known for it's sparkling whites. You might know it as the brand behind the locally loved Nova 7 wine. They have recently been the buzz of the town as they have introduced the alcohol-free, wine-style beverage Piquet Zero, now being sold at grocery stores near you.

### L'Acadie Vineyards
[L'Acadie Vineyards](https://www.lacadievineyards.ca/) Located in the beautiful Annapolis Valley. L'Acadie was one of the first the first organic winery in Nova Scotia and has won many awards for their wines in Canada and Internationally. It is definitely worth a stop in for a tasting session if you can make it!

### Luckett Vineyards
[Luckett Vineyards](https://www.luckettvineyards.com/) Located just north of Windsor, overlooking the Gaspereau Valley. The estate first started growing grapes in 2003, but did not open their doors to the public until 2011, this is a family run vineyard that boasts a lovely restaraunt. A must stop location for wine tasting in Nova Scotia!


## Dessert Shops <a name="desserts"></a>
### Rousseau Chocolatier
[Rousseau Chocolatier](https://rousseauchocolatier.ca) makes one of the best mochas on the city. If you went there, don't forget to order macarons which are quite delicious.

### Cora Breakfast and Lunch
[Cora](https://www.chezcora.com/en/breakfast-lunch-restaurants/cora-clayton-park-halifax/?utm_source=googlemaps&utm_medium=nova-scotia-halifax-lacewood&utm_campaign=website) may be disguised as a family restaurant but do not be fooled. Cora is a desert shop at its heart. Nearly all the dishes have fruit and deserts at the center with meat used as decorations.

### Dairy Bar
[Dairy Bar](https://www.instagram.com/dairybarhfx/?hl=en) is a seasonal ice cream shop on Spring Garden road. It has delicious ice cream, sundaes, and drinks that are worth the wait in their long lines. Dairy Bar is closed in the winter but will be open again in the summer.

### Cacao 70
[Cacao 70](https://cacao70.com/en/our-locations/upper-water-st) is located on the Halifax waterfront, and is an ice cream bar and dessert shop. Open year-round, Cacao 70 is a great place to visit with friends.

### Creamy Rainbow Bakery and Cafe
[Creamy Rainbow Bakery and Cafe](https://g.page/creamy-rainbow-bakery-and-cafe?share) A dessert restaurant in Halifax Downtown. Their egg tarts and puffs are very delicate and delicious.

### Le French Fix Pâtisserie
[Le French Fix Pâtisserie](https://www.lefrenchfix.com/) is a pastry shop located in Prince St. It's a fine looking and smelling French baking. The pastries and croissants are always fresh. A must try if you haven't visted it yet!

### LOS TOROS Auténtico Español
[LOS TOROS Auténtico Español](http://lostoros.ca) is a good spanish restaurant where near the harbour. There are serving the delicious spanish seafood and traditional food. I strongerly recommend the Valencian seafood fried rice.

## Government services <a name="govt"></a>
### City Hall
[City Hall](https://www.halifax.ca/) a home of government of Halifax, specific tourisms are allowed.

### Service Canada
[Service Canada](https://www.servicecanada.info/service-canada-halifax/) is an important place for international students and it is in 1800 Argyle St. If you have visa and tax requirements, you must remember this place.

### Access Nova Scotia
[Access Nova Scotia](https://novascotia.ca/access-locations/) It is probably the only place for you to do the paper work for your motor vehicle. The walk in is now available, you don't have to book an appointment now.

### Government House
[Government House](https://lt.gov.ns.ca/government-house) It is house for governor of Nova Scotia. It is a one of oldest existing government buildings in North America. The house has served for more than 200 years, it is a great Canadian national treasure.

### Halifax Regional Police
[Halifax Regional Police](https://www.google.com/maps/place/Halifax+Regional+Police/@44.6873787,-63.550852,13z/data=!3m1!5s0x4b5a241182537729:0xb6dd7ed87969bbfb!4m9!1m2!2m1!1spolice+station!3m5!1s0x4b5a241178840083:0x2293522ab90b5882!8m2!3d44.6873787!4d-63.5310199!15sCg5wb2xpY2Ugc3RhdGlvbpIBEXBvbGljZV9kZXBhcnRtZW50) Kind and patient help for people's stranded (lost ID etc) son, in Halifax and hopefully on his way back to Ontario. Thanks NS Regional Police dispatcher.

### City of Halifax
[City of Halifax](https://www.halifax.ca/) If you want to pay for your property taxes or any building permits, you can go to City of Halifax. If you are going to get the street parking permits, you can go to City of Halifax.

## Buildings

### Halifax City Hall
[Halifax City Hall](https://www.halifax.ca/city-hall) This Place constructed between 1887 and 1890 and is the largest and one of the oldest municipal building in Nova Scotia. If you are interested in architecture you should visit this place.
![Halifax City Hall](images/Hall.jpeg)

[Image credit](https://www.halifax.ca/city-hall)

### The Vuze
[The Vuze](https://www.templetonproperties.ca/apartments-for-rent-halifax/the-vuze) From it's name "The Vuze", Vuze is also spelt as Views which technically from its name you
can tell its like saying the Views. The views is the tallest building in Halifax. Not just Halifax, it's the tallest building in ALL of Nova
Scotia. It contains 28 floors for normal residents, and 5 pent houses. In total its 33 storeys high above the ground.

### The Jade
[The Jade](https://thejadehalifax.ca/) This is a new building which is located on Barrington St. It is one of the most luxury apartments building in Halifax and has all new facilities. Also, it has a good view of the street or the sea. Here you can enjoy the most comfortable living environment.

### Atlantic Hotel Halifax
[Atlantic Hotel Halifax](https://www.atlanticahotelhalifax.com/) This black and white hotel is built on a bustling and lively street. With comfortable rooms and moderate prices, it has become the best choice of many tourists.
The hotel's excellent location allows people to quickly go to the famous scenic spots in the city. At the same time, you can overlook the night view of the city at night. Customers can experience comfortable treatment at an inexpensive price.

### The Paramount
[The Paramount](https://paramount.universalgroup.ca/) It's an apartment in downtowm of Halifax. It has very convenient transportation. As it's in the downtown so if you want to live here it's very convenient to shop.

### The Peninsula Place
[The Peninsula Place](https://peninsula.universalgroup.ca) The Peninsula Place is located at 1015 Barrington Street, Halifax. It is a nice apartment and very convenient. It is near the Atlantic Superstore and Halifax Railway Station.s

### Park Victoria
[Park Victoria](https://www.capreit.ca/apartments-for-rent/halifax-ns/park-victoria-apartments/) The location of this apartment is excellent, and the price is also very cheap. The apartment is just off Spring Garden Road and is within walking distance of restaurants and various shops.

### Quinpool Tower
[Quinpool Tower](https://killamreit.com/apartments/halifax-ns/quinpool-tower) The Quinpool Tower is located at 2060 Quingate Place, the location of the apartment building is very good especially for students as it is closer to Dalhousie University, SMU and Halifax Commons.

## Libraries

### Halifax Central Library
[Halifax Central Library](https://goo.gl/maps/qKzCZeZMX1jdcRqV8) is a great library. I have been satisfied with each of my visits to the library. the staff and volunteers are very helpful. And using the printing services is always a breeze in the HCL.

### Wallace McCain Learning Commons
[Wallace McCain Learning Commons](https://libraries.dal.ca/hours-locations/wmlc.html) is also one of the dalhousie libraries and it is a very quiet place to study and you can also borrow a computer and book a study room.

### Killam library

[KILLAM LIBRARY](https://www.dal.ca/campus-maps/building-directory/studley-campus/killam-library.html) is my favorite place to study, it is in 6225 University Avenue. Killam library has big space and quiet environment. In particular, you can book study rooms.

### Kellogg Library
[Kellogg Library/Learning Commons](https://libraries.dal.ca/hours-locations/kllc-cheb.html) Another Dalhousie Library in the CHEB on Carleton campus. It is bright and modern and has lots of study spaces and rooms that can be booked.

### Patrick Power Library
[Patrick Power Library](https://goo.gl/maps/s8omy9N22R199qy67) is library of Saint Mary's University. There are sofa in it. It is a peace and comfortable library.

### Keshen Goodman Public Library
[Keshen Goodman Public Library](https://www.halifaxpubliclibraries.ca/locations/kg/) lots of amazing books to borrow, many servsee to help newcomers and students. Has a kids section and section to study. This library is one of my personal libraries to study; libraries in downtown Halifax could get loud sometimes and Keshen Goodman Public Library should be a lot quieter.

### Halifax North Memorial Public Library
[Halifax North Memorial Public Library](https://www.halifaxpubliclibraries.ca/locations/n/) it is also another library in comparision to Halifax Central Library. Closer and more accessible for people who live in the north end.

[Woodlawn Public Library](https://www.halifaxpubliclibraries.ca/locations/W/) is another library located at 31 Eisener Boulevard in Dartmouth. It is a nice spacious library with many study rooms.

### Bedford Public Library
[Bedford Public Library](https://www.halifaxpubliclibraries.ca/locations/BED/) is a library located on Dartmouth located, near the Sunnyside Mall. It's easily accessible through various routes coming from Dartmouth and Bedford. It's a fairly large library with a section for quiet reading along with a section for children to be kept occupied.




## Supermarkets
### Loong 7 Mart
[LOONG 7 MART](https://loong7mart-asian-grocery-store.business.site/?utm_source=gmb&utm_medium=referral) This is a supermarket that specializes in Chinese and Asian food, with a wide variety of Chinese snacks, such as instant noodles,
snail noodles, hot pot base. As well as some great tasting drinks, if you have the opportunity to come and see

### Tai Shan mart
[Tai Shan mart](https://goo.gl/maps/5urytQXNeuFR7Jaz7) it's a Chinese supermarket with a lot of Chinese snacks and daily necessities, I really like to buy hot pot base here.

### M&Y Asian Market
[M&Y Asian Market](https://www.google.com/maps/place/M%26Y%E4%B8%AD%E5%9B%BD%E8%B6%85%E5%B8%82%E5%93%88%E6%B3%95Windsor%E5%BA%97Asian+Market/@44.6453015,-63.5989615,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a21725b25a5c5:0x1d81a6823adc30a6!8m2!3d44.6454221!4d-63.5968456) This is a Chinese supermarket located at 6238 Quinpool Rd. This supermarket has a wide selection of things. The cashiers in this supermarket are very helpful. The goods in this supermarket are good and cheap.

### Topfresh Market
[Topfresh Market](https://goo.gl/maps/UV9Q6knYdMpwATv7A) I think this is the best Chinese supermarket in Halifax. It has a very complete range of Asian food, and the price of meat and vegetables is much cheaper than that in downtown

### Tian Phat Asian Grocery
[Tian Phat Asian Grocery](https://www.yelp.ca/biz/tian-phat-asian-grocery-halifax) Our family go to place for buying asian groceries. Staff are great, and it have most the
ingredient we need.

### V&E's Convenience Plus
[V&E's Convenience Plus](https://ve-convenience-plus.business.site/) is located on 480 Parkland Dr. It's a good Chinese supermarket and most of The dumplings sold in it are handmade, which makes them more delicious than the dumplings sold by others.


### Union Foodmart
[Union Foodmart](https://www.google.ca/maps/place/Union+Foodmart+%E8%8F%AF%E8%81%AF/@44.6729347,-63.5851771,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a21940bd0ecfb:0x5bf091da6ba992c3!8m2!3d44.6729312!4d-63.5829834?hl=en&authuser=0) is located in Dartmouth. The largest Asia Supermarket that I have known.

### South Asian Mart
[South Asian Mart](https://southasianmart.ca/) offers a complete range of ethnic grocery from India, Pakistan, Bangladesh, Sri Lankan at very competitive prices.

### Ca-Hoa Grocery
[Ca-Hoa Grocery](https://goo.gl/maps/2PKXuKs4TddfP1uZA) is an old Asian shop. You can buy some really local Chinese source and cooking tool here.

### OCO mart
[OCO mart](https://www.google.com/maps?q=oco+mart&um=1&ie=UTF-8&sa=X&ved=2ahUKEwjPvZ71n9X3AhWRlYkEHd-eAUEQ_AUoAXoECAIQAw) is a korean grocery store where they have variety of korean products.


### Food & Nutrition
[Food & Nutrition](https://foodnutrition.ca/contact) It not only focus on selling products, but also gives customers a new view on how to know their body well and how to choose and cook foods.

### Shahi Groceries
[Shahi Groceries](https://www.shahigrocery.com/index.php?route=common/home) is a South Asian grocery store. You can get your choice of groceries and snacks from a selection of goods from your home country.

### Baileys Meat Market
[Baileys Meat Market](https://baileys-meat-market.business.site/?utm_source=gmb&utm_medium=referral) This is a store where Beef , Chicken and Lamb meat is available along with loads of spices and snacks and food items or drinks from Arab countries and also Pakistan and India.

## Shopping Centers <a name="shopping"></a>
### Halifax Shopping Centre
[Halifax Shopping Centre](https://halifaxshoppingcentre.com/) The biggest shopping center in Halifax. You can find all kinds of brands there and buy anything you want. Also, there is a Walmart which is opposite to it.

### Mic Mac Mall
[Mic Mac Mall](https://micmacmall.com/) The biggest shopping center in Dartmouth, it offers you the opportunity to shop everything in Dartmouth without crossing the bridge.

### IKEA Halifax
[IKEA Halifax](https://www.ikea.com/ca/en/stores/halifax/?utm_source=google&utm_medium=organic&utm_campaign=map&utm_content=halifax) the biggest shopping center about furniture in Dartmouth(like shopping center but not exactly), if you want the newest furniture, then you can go there and find if some of them are suitable for you.

### Park lane mall
[park lane mall](http://shopparklane.ca/) a place to go shopping, watching movies, and browsing dollarama with your friends during the weekend

### Scotia Square
[Scotia Square](https://scotiasquare.com/) is a shopping centre near the Halifax Waterfront. It is probably best known for its large food court, featuring a variety of different restaurants. Connected to Scotia Square is a bus terminal for Halifax Transit, this makes Scotia Square a hub of sorts for those arriving in Halifax from areas like Dartmouth, giving greater attention to the shops and restaurants featured within. The close proximity to other areas such as the waterfront and Citadel Hill also make this a nice place to visit.

### Sunnyside Mall
[Sunnyside Mall](https://sunnysidemall.ca/) This is one of the 2 malls located in Bedford Nova Scotia. Its a 30 minute drive from halifax but i wouldnt recommend going here as you can find everything located in this mall also in the HSC.

### Winners
[Winners](https://www.winners.ca/en) is a good place for shopping because everything is cheap with high quality. New cloth and designer fashions arrive several times a week.

### The Black Market Boutique
[The Black Market Boutique](https://www.blackmarkethalifax.ca/) is an abundantly colorful store located on Grafton St. featuring crafts and goods from around the world.

### The Loot
[The Loot](https://www.instagram.com/theloothfx/?hl=en) is a curated thrift store selling vintage clothing, accessories, and art.

### The Halifax Army Navy Store
[The Halifax Army Navy Store](http://www.thehalifaxarmynavystore.net/), (also known as Ron's Army/Navy), is a family owned military surplus store located on Agricola St.

### Strange Adventures: Comics & Curiosities
[Strange Adventures: Comics & Curiosities](http://www.strangeadventures.com/) Canada's oddest and award-winning comic book store, located in both downtown Halifax and downtown Dartmouth. Selling comics since 1992.

### Pete's Frootique
[Pete's Frootique](https://petes.ca/) is a grocery store with locations at Dresden Row in Halifax, as well as the Sunnyside Mall in Bedford. It has a wide selection of products that are hard to find elsewhere in other, larger grocery stores, including many international products from areas such as Great Britain. It also features a juice bar which serves many different smoothies and juices.

### Value Village
[Value Village](https://stores.savers.com/ns/halifax/valuevillage-thrift-store-2046.html) is a thrift store where you can find essentiall everything. From electronics to vintage cloth, you can find anything at Value village.

### Jennifer's of Nova Scotia
[Jennifer's of Nova Scotia](https://www.jennifers.ns.ca/) is a locally-owned gift shop. It exclusively carries locally-crafted products meant to share the Atlantic Canadian experience.

### Oldest McDonald's
[McDonald's Quinpool](https://www.mcdonalds.com/ca/en-ca/location/halifax/halifax-quinpool/6324-quinpool-road-/5652.html) is one of the busiest stores and is the first ever McDonald's in Halifax which is open 24*7.

### Walmart
[Walmart](https://www.walmart.ca/en) is one of the largest grocery stores in Halifax. You can find all kinds of groceries as well as electronics, home appliances, and stuff like curtains, cushions, and duvets. All at an affordable price.

### The Monster Comic Lounge
[Monster Comic Lounge](https://www.facebook.com/MonsterComicLounge/) is a collectibles store located on Gottingen St. Here, you can purchase and exchange trading cards. Also, you can find a variety of other collectible items including figures, board games and comic books.

### Atlantic Photo Supply
[Atlantic Photo Supply](https://www.atlanticphotosupply.com/) is the last retail store in Halifax that still develops colour film on site. If you need to get your camera film developed I reccomend them for fair prices and speedy development times. They also sell camera equipment and prints if your into that sort of thing.

### Dartmouth Crossing
[Dartmouth Crossing](https://www.google.com/maps/place/Dartmouth+Crossing,+Dartmouth,+NS/@44.7013455,-63.576132,15z/data=!3m1!4b1!4m5!3m4!1s0x4b5a26bef921f91d:0xd31163dc6cc44ac8!8m2!3d44.7055743!4d-63.5637721)
Not really a shopping centre it is; however, I am sure you will want to visit at least one of the places here. There are lots of places to get new clothes for the Summer, as well as Costco and Ikea are there too! Plus if you get tired and hungry while you are shopping, there are great food places such as Montana's or Boston Pizza as well.

## Academic institutions <a name="universities"></a>

### Dalhousie University

![Dalhousie University](images/Dal.PNG)

[Image credit](https://signalhfx.ca/dalhousie-university-confirms-jan-31-resumption-of-in-person-classes/)

[Dalhousie University](https://www.tripadvisor.ca/Attraction_Review-g154976-d677952-Reviews-Dalhousie_University-Halifax_Halifax_Regional_Municipality_Nova_Scotia.html) is a public research university that is more than 200 years old. Some of its historic infrastructures are worth visiting. Find more information about the university [here](https://www.dal.ca/).

### King's College
[University of King's College](https://ukings.ca/)
located near and working in collaboration with Dalhousie University. Located at 6350 Coburg Rd, Halifax.

### Mount Saint Vincet University
[Mount Saint Vincet University](https://www.msvu.ca/) A Primary Undergraduate university which locate at 166 Bedford Hwy, Halifax.

### Saint Mary's University
[Saint Mary's University](https://www.smu.ca/) A Primary Undergraduate university which locate at 923 Robie St near dalhousie university.

### Acadia University
[Acadia University](https://www2.acadiau.ca/home.html) A public undergraduate university ranked among the top Canadian institutes is located in Wolfville, N.S.

### Nova Scotia Community College
[Nova Scotia Community College](https://www.nscc.ca/) Nova Scotia Community College is located at the end of Barrington St. It contains 130 programs at 14 different campuses in Nova Scotia. NSCC is one of the known Univerisities found in Halifax as it allows their students to be flexible and offers a lot.

### NSCAD University
[NSCAD University](https://nscad.ca/) also called the Nova Scotia College of Art and Design, is a post-secondary art school in Halifax, Nova Scotia, Canada. NSCAD offers an interdisciplinary university experience unlike any other art school in the country.

### Université Sainte-Anne
[Université Sainte-Anne](https://www.usainteanne.ca/campus/halifax) Université Sainte-Anne is a french-language university located in 1190 Barrington St.

### Cape Breton University
[Cape Breton University](https://www.cbu.ca/) Cape breton University is located in Sydney Cape Breton. It is the only post secondary institution located inside Cape Breton.

### St. Francis Xavier University
[St. Francis Xavier University](https://www.stfx.ca/) is an undergraduate university located in Antigonish, Nova Scotia.









## Learning Opportunities <a name="learning"></a>
### Halifax Central Library Media Studios
[The Halifax Central Library Media Studios](https://www.halifaxpubliclibraries.ca/library-spaces/book-a-space/media-studio/) comes well-equipped with musical instruments, recording equipment, and top-quality software. You can even do video editing, since it has a green-screen in one of the studios. To top it off, you can rent out equipment, and learn various things through your library card which has access to LinkedIn Learning; and ALL of this is FREE!

### Here We Code
[Here We Code](https://www.herewecode.ca/) movement is about collaboration between industry, school for the evergrowing economic and social future for all communities in Nova Scotia.

## Farmers Markets <a name="farmers"></a>
### Bedford basin Farmers Market
[Bedford basin Farmers Market](https://www.bedfordbasinmarket.com/) Greek owned farmers market, with a bistro, spice market, and imported goods from Europe (Mainly
Greece and Germany)

### Halifax Seaport Market
[Halifax Seaport Market](https://www.halifaxfarmersmarket.com/)
The Halifax Seaport Farmers' Market was created in 1750 and is the oldest,
continuously operating farmers' market in North America. In July 2022 The
Halifax Seaport Farmers' Market will be moving to the west end of Pavilion 23.



## Transportation
Convenient access to transportation is important to commute around, visit places and access amenities. There are many ways of transportaion available in Halifax which will help you to get around different parts of the city.

### Taxis
![Taxi](images/taxi.jpg)

Taxis are readily available in downtown Halifax, Dartmouth and throughout the neighbourhoods. Uber services have also started recently in Halifax which makes commutation even cheaper. Some of the taxi services include [Casino Taxi](https://www.casinotaxi.ca "Casino Taxi"), [Yellow Cab](https://yellowcabhalifax.ca "Yellow Cab") and [Halifax Airport Taxi & Limosine Service](https://halifaxairporttaxi.ca "Provided by Air Rider Taxi & Limo")

### Ferries

![Ferry](images/Ferry.jpg)

[Ferries](https://www.halifax.ca/transportation/halifax-transit/ferry-service "Ferry Service") are operated by Halifax Transit, a public transport service run by the government. A 15-minute ride between Halifax to Downtown Dartmouth at just $2.75 is a must to do experience for visitors and travellers.

### Halifax Transit Buses
![Bus](images/Bus.jpg)

Halifax Transit also provides [Transit bus](https://www.halifax.ca/transportation/halifax-transit/routes-schedules "Halifax Transit Bus Service") service throughout Halifax region. There are 353 buses owned by Halifax Transit and they run from early morning to past midnight. The best thing about Halifax Transit Service is that the same transfer ticket can be used in both ferry and bus for commuting.
