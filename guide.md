# Explore Halifax

![Halifax waterfront](images/halifax.png)

> CSCI 3130 (Software Engineering) is one of the courses that I (Tushar Sharma) teach at Dalhousie University. As a part of the course, I took a lecture on git and GitHub. After the session, I gave the students an assignment - to contribute to a GitHub repository i.e., "Explore Halifax". In the assignment, each student had to write about the city they live in. My primary goal was to help them understand how they can use git and GitHub (since they supposed to contribute to a single file in the repository). As a (pleasant) side effect, the assignment has prepared a comprehensive list of things to do/see/experience in Halifax, NS, Canada.

You may find the [open-source repository here](https://gitlab.com/daluniversity/explore-halifax); you are welcome to contribute more.
Thanks to all students and contributors who made this list awesome.
